<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/css/style.css">
  <title>Document</title>
</head>

<body>

  <?php
    date_default_timezone_set('Asia/Yerevan');
    $db = mysqli_connect("localhost", "root", "root", "form");

    if (isset($_POST['submit'])) {
      $image = $_FILES['inpFile']['name'];
      $title = $_POST['inpTitle'];
      $color = $_POST['inpColor'];
      $created_at = date('Y-m-d H:i:s');
      $fileExt = explode('.', $image);
      $fileActualExt = strtolower(end($fileExt));
      $allowed = array("jpg", "jpeg", "png", "gif");

      $fileNameNew = uniqid('', true) . "." . $fileActualExt;
      if (in_array($fileActualExt, $allowed)) {
        $sql = "INSERT INTO images (path,title,color,created_at) VALUES ('$fileNameNew','$title','$color','$created_at')";
      }

      mysqli_query($db, $sql);
      $fileDestination = 'images/' . $fileNameNew;

      move_uploaded_file($_FILES['inpFile']['tmp_name'], $fileDestination);
    }

    if (isset($_POST["save"])) {
      $editTitle = $_POST['editTitle'];
      $editColor = $_POST['editColor'];
      $id = $_POST['id'];

      $sql = "UPDATE images SET title = '$editTitle', color = '$editColor' WHERE id=$id";

      mysqli_query($db, $sql);
    }

    $arr = "SELECT * FROM images";
    $result = $db->query($arr);

    if (isset($_POST['delete'])) {
      $arrImages = mysqli_fetch_assoc($result);
      $id = $_POST['id'];
      $sql = "DELETE FROM images WHERE id=$id";
      mysqli_query($db, $sql);
      if (!empty($arrImages["path"])) {
        unlink("images/" . $arrImages["path"]);
      }
    }

    $items = mysqli_fetch_all($result, MYSQLI_ASSOC);

  ?>





  <?php foreach ($items as $item) { ?>

      <div class="changes">
        <form action="index.php" class="changesForm" method="post">
          <input type="hidden" name="id" value="<?php echo $item['id'] ?>">
          <img src="images/<?php echo $item['path'] ?>" class="img">
          <input type="text" value="<?php echo $item['title'] ?>" name="editTitle" class="container__inp">
          <input type="text" value="<?php echo $item['color'] ?>" name="editColor" class="container__inp">
          <input type="submit" value="Save" name="save" class="container__inp container-inp--green">
          <input type="submit" value="X" name="delete" class="container__inp container-inp--red">
        </form>
      </div>

  <?php } ?>

  <form action="index.php" method="post" enctype="multipart/form-data">
    <div class="container">
      <input type="file" name="inpFile" class="container__inp">
      <input type="text" name="inpTitle" placeholder="title" class="container__inp">
      <input type="text" name="inpColor" placeholder="color" class="container__inp">
      <input type="submit" name="submit" value="Add" class="container__inp container-inp--green">
    </div>
  </form>

  <ul class="masonry">
    <?php foreach($items as $item) { ?>
        <li class="masonry__item"><img src="images/<?php echo $item['path'] ?>" alt="1" class="masonry__image" style=" box-shadow: 0 0 10px 5px <?php echo $item['color'] ?>" ></li>
    <?php } ?>
  </ul>


<script> 
  window.addEventListener('resize', function () {
  if (Masonry.prototype.resize) {
    Masonry.render()
  }
})

function Masonry(selector, size) {
  Masonry.prototype.selector = selector
  Masonry.prototype.width = size.width
  Masonry.prototype.resize = size.resize

  Masonry.render = function () {
    const images = Array.from(document.querySelectorAll(`${Masonry.prototype.selector} li`));
    let images__height = [];
    let widths__sum = 0;
    let min__height = 0;
    let image__margin = 0;
    let min__height__index = 0;

    images.map((img) => {
      img.style.width = `${Masonry.prototype.width}px`;
      widths__sum += Masonry.prototype.width;
      if (widths__sum <= window.innerWidth) {
        images__height.push(img.offsetHeight);
        img.style.cssText = `width:${Masonry.prototype.width}px; left:${widths__sum-Masonry.prototype.width}px`;
        return
      }
      min__height = Math.min.apply(null, images__height);
      min__height__index = images__height.indexOf(min__height);
      image__margin = images[min__height__index].style.left;

      img.style.cssText =
        `
        top:${min__height}px; 
        left:${image__margin};
        width:${Masonry.prototype.width}px
    
      `
      images__height[min__height__index] += img.offsetHeight;
    })
  }
}


Masonry('.masonry', {
  width: 200,
  resize: true
});
Masonry.render();


</script>


</body>

</html>